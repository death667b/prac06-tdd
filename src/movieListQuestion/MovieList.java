/**
 * 
 */
package movieListQuestion;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author user
 *
 */
public class MovieList {
	private TreeMap<String, Integer> movie;
	
	public MovieList(){
		movie = new TreeMap<String, Integer>();
	}
	
	public void addMovie(String movieName) throws MovieListException {
		if (movie.containsKey(movieName)){
			throw new MovieListException("Movie already exist in database.");
		}
		
		movie.put(movieName, -1);
	}

	public String getRating(String movieName) throws MovieListException {
		String returnString = "";
		int rating;
		
		if (!movie.containsKey(movieName)){
			throw new MovieListException("Movie does not exist in database.");
		}
		
		rating = movie.get(movieName);
		
		if (rating != -1){
			for (int i = 0; i < rating; i++){
				returnString += "*";
			}
		} else {
			returnString = "No rating";
		}

		return returnString;
	}

	public void setRating(String movieName, int rating) throws MovieListException {
		if (rating < 1 || rating > 5){
			throw new MovieListException("Rating needs to be between 1 and 5");
		}
		
		if (!movie.containsKey(movieName)){
			throw new MovieListException("Movie does not exist in database.");
		}
		
		movie.put(movieName, rating);
	}

	public String getList() {
	String returnString = "";
	
	for (Map.Entry<String, Integer> entry : movie.entrySet()) {
		String key = entry.getKey();
		returnString += key + "\n";
	}
		
	return returnString;
	}

}
